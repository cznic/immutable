module modernc.org/immutable

go 1.15

require (
	modernc.org/mathutil v1.1.1 // indirect
	modernc.org/strutil v1.1.0
)
