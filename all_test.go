// Copyright 2020 The Immutable Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package immutable

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"path"
	"runtime"
	"runtime/debug"
	"strings"
	"testing"
	"time"

	"modernc.org/strutil"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func stack() string { return string(debug.Stack()) }

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO, stack, dump) //TODOOK
}

// ----------------------------------------------------------------------------

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func TestCar(t *testing.T) {
	if g, e := Car(nil), interface{}(nil); g != e {
		t.Fatal(g, e)
	}

	if g, e := Car(Cons(42, nil)), 42; g != e {
		t.Fatal(g, e)
	}
}

func TestCdr(t *testing.T) {
	if g, e := Cdr(nil), (*List)(nil); g != e {
		t.Fatal(g, e)
	}

	if g, e := Cdr(Cons(42, nil)), (*List)(nil); g != e {
		t.Fatal(g, e)
	}

	if g := Cdr(Cons(42, Cons(314, nil))); g == nil || Car(g) != 314 || Cdr(g) != nil {
		t.Fatal()
	}
}

func TestCons(t *testing.T) {
	g := Cons(42, nil)
	if g == nil || Car(g) != 42 || Cdr(g) != nil {
		t.Fatal()
	}
}

func TestNewList(t *testing.T) {
	// clisp 2.49.92

	for itest, test := range []struct {
		args []interface{}
		s    string
	}{
		// [1]> (list)
		// NIL
		{nil, "[]"},
		// [2]> (list 42)
		// (42)
		{[]interface{}{42}, "[42]"},
		// [3]> (list nil)
		// (NIL)
		{[]interface{}{nil}, "[<nil>]"},
		// [4]> (list nil nil)
		// (NIL NIL)
		{[]interface{}{nil, nil}, "[<nil> <nil>]"},
		// [5]> (list nil 42)
		// (NIL 42)
		{[]interface{}{nil, 42}, "[<nil> 42]"},
		// [6]> (list 42 nil)
		// (42 NIL)
		{[]interface{}{42, nil}, "[42 <nil>]"},
		// [7]> (list 42 314)
		// (42 314);
		{[]interface{}{42, 314}, "[42 314]"},
	} {
		g, e := NewList(test.args...).String(), test.s
		t.Log(itest+1, test.args, g)
		if g != e {
			t.Fatalf("%v: %q %q", itest, g, e)
		}

	}
}

func TestAppend(t *testing.T) {
	// clisp 2.49.92

	for itest, test := range []struct {
		args []interface{}
		s    string
		err  bool
	}{
		// [1]> (append)
		// NIL
		{nil, "[]", false},
		// [2]> (append nil)
		// NIL
		{[]interface{}{NewList()}, "[]", false},
		// [3]> (append 42)
		// 42
		{[]interface{}{42}, "42", false},
		// [4]> (append (list))
		// NIL
		{[]interface{}{NewList()}, "[]", false},
		// [5]> (append (list 42))
		// (42)
		{[]interface{}{NewList(42)}, "[42]", false},
		// [6]> (append nil nil)
		// NIL
		{[]interface{}{NewList(), NewList()}, "[]", false},
		// [7]> (append nil 42)
		// 42
		{[]interface{}{NewList(), 42}, "42", false},
		// [8]> (append nil (list))
		// NIL
		{[]interface{}{NewList(), NewList()}, "[]", false},
		// [9]> (append nil (list 42))
		// (42)
		{[]interface{}{NewList(), NewList(42)}, "[42]", false},
		// [10]> (append (list 42) nil)
		// (42)
		{[]interface{}{NewList(42), NewList()}, "[42]", false},
		// [11]> (append (list 42) 314)
		// (42 . 314)
		{[]interface{}{NewList(42), 314}, "[42 314]", false},
		// [12]> (append (list 42) (list 314))
		// (42 314)
		{[]interface{}{NewList(42), NewList(314)}, "[42 314]", false},
		// [13]> (append nil nil nil)
		// NIL
		{[]interface{}{NewList(), NewList(), NewList()}, "[]", false},
		// [14]> (append nil nil 314)
		// 314
		{[]interface{}{NewList(), NewList(), 314}, "314", false},
		// [15]> (append nil nil (list 42))
		// (42)
		{[]interface{}{NewList(), NewList(), NewList(42)}, "[42]", false},
		// [16]> (append nil (list 42) nil)
		// (42)
		{[]interface{}{NewList(), NewList(42), NewList()}, "[42]", false},
		// [17]> (append nil (list 42) 314)
		// (42 . 314)
		{[]interface{}{NewList(), NewList(42), 314}, "[42 314]", false},
		// [18]> (append nil (list 42) (list 314))
		// (42 314)
		{[]interface{}{NewList(), NewList(42), NewList(314)}, "[42 314]", false},
		// [19]> (append (list 42) nil nil)
		// (42)
		{[]interface{}{NewList(42), NewList(), NewList()}, "[42]", false},
		// [20]> (append (list 42) nil 314)
		// (42 . 314)
		{[]interface{}{NewList(42), NewList(), 314}, "[42 314]", false},
		// [21]> (append (list 42) nil (list 314))
		// (42 314)
		{[]interface{}{NewList(42), NewList(), NewList(314)}, "[42 314]", false},
		// [22]> (append (list 42) (list 314) nil)
		// (42 314)
		{[]interface{}{NewList(42), NewList(314), NewList()}, "[42 314]", false},
		// [23]> (append (list 42) (list 314) 278)
		// (42 314 . 278)
		{[]interface{}{NewList(42), NewList(314), 278}, "[42 314 278]", false},
		// [24]> (append (list 42) (list 314) (list 278))
		// (42 314 278)
		{[]interface{}{NewList(42), NewList(314), NewList(278)}, "[42 314 278]", false},

		// errors

		// (append 42 nil)
		// *** - APPEND: 42 is not a list
		{[]interface{}{42, NewList()}, "", true},
		// (append 42 314)
		// *** - APPEND: 42 is not a list
		{[]interface{}{42, 314}, "", true},
		// *** - APPEND: 42 is not a list
		// (append 42 (list 314))
		{[]interface{}{42, NewList(314)}, "", true},
		// (append nil 42 nil)
		{[]interface{}{NewList(), 42, NewList()}, "", true},
		// (append nil 42 314)
		{[]interface{}{NewList(), 42, 314}, "", true},
		// (append nil 42 (list 314))
		{[]interface{}{NewList(), 42, NewList(314)}, "", true},
	} {
		g, err := Append(test.args...)
		t.Log(itest+1, test.args, g, err)
		if err != nil {
			if g != nil {
				t.Fatal(g, err)
			}

			if !test.err {
				t.Fatal(g, err)
			}

			continue
		}

		if g, e := fmt.Sprint(g), test.s; g != e {
			t.Fatalf("%v: %q %q", itest, g, e)
		}
	}
}

func TestLast(t *testing.T) {
	for itest, test := range []struct {
		arg *List
		n   int
		s   string
	}{
		{NewList(), 0, "[]"},
		{NewList(), 1, "[]"},

		{NewList(nil), 0, "[]"},
		{NewList(nil), 1, "[<nil>]"},
		{NewList(nil), 2, "[<nil>]"},

		{NewList(42), 0, "[]"},
		{NewList(42), 1, "[42]"},
		{NewList(42), 2, "[42]"},

		{NewList(NewList()), 0, "[]"},
		{NewList(NewList()), 1, "[[]]"},
		{NewList(NewList()), 2, "[[]]"},

		{NewList(nil, nil), 0, "[]"},
		{NewList(nil, nil), 1, "[<nil>]"},
		{NewList(nil, nil), 2, "[<nil> <nil>]"},
		{NewList(nil, nil), 3, "[<nil> <nil>]"},

		{NewList(nil, 42), 0, "[]"},
		{NewList(nil, 42), 1, "[42]"},
		{NewList(nil, 42), 2, "[<nil> 42]"},
		{NewList(nil, 42), 3, "[<nil> 42]"},

		{NewList(nil, NewList(42)), 0, "[]"},
		{NewList(nil, NewList(42)), 1, "[[42]]"},
		{NewList(nil, NewList(42)), 2, "[<nil> [42]]"},
		{NewList(nil, NewList(42)), 3, "[<nil> [42]]"},

		{NewList(42, nil), 0, "[]"},
		{NewList(42, nil), 1, "[<nil>]"},
		{NewList(42, nil), 2, "[42 <nil>]"},
		{NewList(42, nil), 3, "[42 <nil>]"},

		{NewList(42, 314), 0, "[]"},
		{NewList(42, 314), 1, "[314]"},
		{NewList(42, 314), 2, "[42 314]"},
		{NewList(42, 314), 3, "[42 314]"},

		{NewList(42, NewList(314)), 0, "[]"},
		{NewList(42, NewList(314)), 1, "[[314]]"},
		{NewList(42, NewList(314)), 2, "[42 [314]]"},
		{NewList(42, NewList(314)), 3, "[42 [314]]"},

		{NewList(NewList(42), nil), 0, "[]"},
		{NewList(NewList(42), nil), 1, "[<nil>]"},
		{NewList(NewList(42), nil), 2, "[[42] <nil>]"},
		{NewList(NewList(42), nil), 3, "[[42] <nil>]"},

		{NewList(NewList(42), 314), 0, "[]"},
		{NewList(NewList(42), 314), 1, "[314]"},
		{NewList(NewList(42), 314), 2, "[[42] 314]"},
		{NewList(NewList(42), 314), 3, "[[42] 314]"},

		{NewList(NewList(42), NewList(314)), 0, "[]"},
		{NewList(NewList(42), NewList(314)), 1, "[[314]]"},
		{NewList(NewList(42), NewList(314)), 2, "[[42] [314]]"},
		{NewList(NewList(42), NewList(314)), 3, "[[42] [314]]"},

		{NewList(1, 2, 3), 0, "[]"},
		{NewList(1, 2, 3), 1, "[3]"},
		{NewList(1, 2, 3), 2, "[2 3]"},
		{NewList(1, 2, 3), 3, "[1 2 3]"},
		{NewList(1, 2, 3), 4, "[1 2 3]"},

		{NewList(1, 2, 3, 4), 0, "[]"},
		{NewList(1, 2, 3, 4), 1, "[4]"},
		{NewList(1, 2, 3, 4), 2, "[3 4]"},
		{NewList(1, 2, 3, 4), 3, "[2 3 4]"},
		{NewList(1, 2, 3, 4), 4, "[1 2 3 4]"},
		{NewList(1, 2, 3, 4), 5, "[1 2 3 4]"},

		{NewList(1, 2, 3, 4, 5), 0, "[]"},
		{NewList(1, 2, 3, 4, 5), 1, "[5]"},
		{NewList(1, 2, 3, 4, 5), 2, "[4 5]"},
		{NewList(1, 2, 3, 4, 5), 3, "[3 4 5]"},
		{NewList(1, 2, 3, 4, 5), 4, "[2 3 4 5]"},
		{NewList(1, 2, 3, 4, 5), 5, "[1 2 3 4 5]"},
		{NewList(1, 2, 3, 4, 5), 6, "[1 2 3 4 5]"},
	} {
		g := Last(test.arg, test.n)
		t.Log(itest+1, test.arg, test.n, g)
		if g, e := fmt.Sprint(g), test.s; g != e {
			t.Fatalf("%v: %q %q", itest, g, e)
		}
	}
}

func TestMember(t *testing.T) {
	for itest, test := range []struct {
		m    int
		list *List
		s    string
	}{
		{42, NewList(), "[]"},
		{42, NewList(314), "[]"},
		{42, NewList(42, 314), "[42 314]"},
		{42, NewList(422, 314), "[]"},
		{42, NewList(314, 42), "[42]"},
		{42, NewList(314, 422), "[]"},
		{42, NewList(42, 314, 42), "[42 314 42]"},
		{42, NewList(422, 314, 42), "[42]"},
		{42, NewList(422, 314, 422), "[]"},
		{42, NewList(278, 42, 314), "[42 314]"},
		{42, NewList(278, 422, 314), "[]"},
		{42, NewList(278, 314, 42), "[42]"},
		{42, NewList(278, 314, 422), "[]"},
	} {
		g := Member(test.m, func(a, b interface{}) bool { return a == b }, test.list)
		t.Log(itest+1, test.m, test.list, g)
		if g, e := fmt.Sprint(g), test.s; g != e {
			t.Fatalf("%v: %q %q", itest, g, e)
		}
	}
}

func TestReverse(t *testing.T) {
	for itest, test := range []struct {
		list *List
		s    string
	}{
		{NewList(), "[]"},
		{NewList(1), "[1]"},
		{NewList(1, 2), "[2 1]"},
		{NewList(1, 2, 3), "[3 2 1]"},
		{NewList(1, 2, 3, 4), "[4 3 2 1]"},
		{NewList(1, 2, 3, 4, 5), "[5 4 3 2 1]"},
	} {
		g := Reverse(test.list)
		t.Log(itest+1, test.list, g)
		if g, e := fmt.Sprint(g), test.s; g != e {
			t.Fatalf("%v: %q %q", itest, g, e)
		}
	}
}

func BenchmarkAppend(b *testing.B) {
	n := 10
	for e := 1; e < 7; e++ {
		b.Run(fmt.Sprintf("1e%d", e), func(b *testing.B) {
			benchmarkAppend(b, n)
		})
		n *= 10
	}
}

func benchmarkAppend(b *testing.B, n int) {
	var err error
	var l2 interface{}
	b.ReportAllocs()
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l := Cons(0, nil)
		for j := 1; j < n; j++ {
			if l2, err = Append(l, j); err != nil {
				b.Fatal(err)
			}

			l = l2.(*List)
		}
	}
}

func BenchmarkCons(b *testing.B) {
	n := 10
	for e := 1; e < 7; e++ {
		b.Run(fmt.Sprintf("1e%d", e), func(b *testing.B) {
			benchmarkCons(b, n)
		})
		n *= 10
	}
}

func benchmarkCons(b *testing.B, n int) {
	b.ReportAllocs()
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var l *List
		for j := 0; j < n; j++ {
			l = Cons(j, l)
		}
	}
}

func BenchmarkSnoc(b *testing.B) {
	n := 10
	for e := 1; e < 7; e++ {
		b.Run(fmt.Sprintf("1e%d", e), func(b *testing.B) {
			benchmarkSnoc(b, n)
		})
		n *= 10
	}
}

func benchmarkSnoc(b *testing.B, n int) {
	b.ReportAllocs()
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var l *List
		for j := 0; j < n; j++ {
			l = Snoc(l, j)
		}
	}
}

func TestAppendImmutability(t *testing.T) {
	for itest, test := range []struct {
		l0, l1, l2 *List
	}{
		{NewList(1), NewList(2), NewList(1, 2)},
		{NewList(1), NewList(2, 3), NewList(1, 2, 3)},
		{NewList(1, 2), NewList(3), NewList(1, 2, 3)},
		{NewList(1, 2), NewList(3, 4), NewList(1, 2, 3, 4)},
		{NewList(1), NewList(2, 3, 4), NewList(1, 2, 3, 4)},
		{NewList(1, 2), NewList(3, 4), NewList(1, 2, 3, 4)},
		{NewList(1, 2, 3), NewList(4), NewList(1, 2, 3, 4)},
	} {
		g0 := test.l0.String()
		l, err := Append(test.l0, test.l1)
		if err != nil {
			t.Fatalf("%v: Append: %v", itest, err)
		}

		if g, e := test.l0.String(), g0; g != e {
			t.Fatalf("%v: l0 mutated: %v %v", itest, g, e)
		}

		if g, e := l.(*List).String(), test.l2.String(); g != e {
			t.Fatalf("%v: l2 wrong: %q %q", itest, g, e)
		}
	}
}

func TestConsImmutability(t *testing.T) {
	l := NewList()
	l1 := Cons(1, l)
	l2 := Cons(2, l)
	if g, e := l.String(), "[]"; g != e {
		t.Fatalf("l mutated: %q %q", g, e)
	}

	if g, e := l1.String(), "[1]"; g != e {
		t.Fatalf("l1 wrong: %q %q", g, e)
	}

	if g, e := l2.String(), "[2]"; g != e {
		t.Fatalf("l1 wrong: %q %q", g, e)
	}

	l = NewList(42)
	l1 = Cons(1, l)
	l2 = Cons(2, l)
	if g, e := l.String(), "[42]"; g != e {
		t.Fatalf("l mutated: %q %q", g, e)
	}

	if g, e := l1.String(), "[1 42]"; g != e {
		t.Fatalf("l1 wrong: %q %q", g, e)
	}

	if g, e := l2.String(), "[2 42]"; g != e {
		t.Fatalf("l1 wrong: %q %q", g, e)
	}
}

func TestReverseImmutability(t *testing.T) {
	for itest, test := range []struct {
		l0, l1 *List
	}{
		{NewList(), NewList()},
		{NewList(1), NewList(1)},
		{NewList(1, 2), NewList(2, 1)},
		{NewList(1, 2, 3), NewList(3, 2, 1)},
		{NewList(1, 2, 3, 4), NewList(4, 3, 2, 1)},
	} {
		g0 := test.l0.String()
		l1 := Reverse(test.l0)
		if g, e := test.l0.String(), g0; g != e {
			t.Fatalf("%v: l0 mutated: %v %v", itest, g, e)
		}

		if g, e := l1.String(), test.l1.String(); g != e {
			t.Fatalf("%v: l1 wrong: %q %q", itest, g, e)
		}
	}
}

func TestSnoc(t *testing.T) {
	for itest, test := range []struct {
		l0, l1 *List
	}{
		{NewList(), NewList(1)},
		{NewList(2), NewList(2, 1)},
		{NewList(3, 2), NewList(3, 2, 1)},
	} {
		g0 := test.l0.String()
		l1 := Snoc(test.l0, 1)
		if g, e := test.l0.String(), g0; g != e {
			t.Fatalf("%v: l0 mutated: %v %v", itest, g, e)
		}

		if g, e := l1.String(), test.l1.String(); g != e {
			t.Fatalf("%v: l1 wrong: %q %q", itest, g, e)
		}
	}
}

func TestSnoc2(t *testing.T) {
	l := NewList()
	l = Snoc(l, 1)
	l = Snoc(l, 2)
	l = Snoc(l, 3)
	t.Log(l)
	for i, v := range []interface{}{1, 2, 3, nil} {
		if g, e := Car(l), v; g != e {
			t.Fatal(i, g, e)
		}

		l = Cdr(l)
	}
}

func TestSnoc3(t *testing.T) {
	l := NewList(1)
	l = Snoc(l, 2)
	l = Snoc(l, 3)
	l = Cons(9, l)
	l = Snoc(l, 4)
	l = Snoc(l, 5)
	t.Log(l)
	for i, v := range []interface{}{9, 1, 2, 3, 4, 5, nil} {
		if g, e := Car(l), v; g != e {
			t.Fatal(i, g, e)
		}

		l = Cdr(l)
	}
}

func dump(l *List) string {
	var b strings.Builder
	f := strutil.IndentFormatter(&b, "· ")
	dump0(f, l)
	return b.String()
}

func dump0(f strutil.Formatter, l *List) {
	if l == nil {
		f.Format("<nil>")
		return
	}

	f.Format("{%i\n")
	f.Format("car: %v,\n", l.car)
	f.Format("cdr: ")
	switch {
	case l.cdr == nil:
		f.Format("<nil>")
	default:
		dump0(f, l.cdr)
	}
	f.Format("\n")
	f.Format("append: %v", l.append)
	f.Format("%u\n}")
}

func rndElement(rng *rand.Rand) interface{} {
	if r := rng.Intn(11); r < 10 {
		return r
	}

	return nil
}

func rndList(rng *rand.Rand) *List {
	n := rng.Intn(4)
	var a []interface{}
	for i := 0; i < n; i++ {
		a = append(a, rndElement(rng))
	}
	return NewList(a...)
}

func TestSnocElemPersistent(t *testing.T) {
	const N = 1e6
	l0 := NewList()
	m := map[*List]string{l0: l0.String()}
	a := []*List{l0}
	rng := rand.New(rand.NewSource(time.Now().Local().UnixNano()))

	register := func(l *List) *List {
		switch e, ok := m[l]; {
		case ok:
			if g := l.String(); g != e {
				t.Fatalf("%v: %p %q %q", len(a), l, g, e)
			}
		default:
			m[l] = l.String()
			a = append(a, l)
		}
		return l
	}

	for len(a) < N {
		l := a[rng.Intn(len(a))]
		e := rndElement(rng)
		l2 := Snoc(l, e)
		register(l2)
	}
	for l, e := range m {
		if g := l.String(); g != e {
			t.Fatalf("%v: %p %q %q", len(a), l, g, e)
		}
	}
}

func TestSnocListPersistent(t *testing.T) {
	const N = 1e6
	l0 := NewList()
	m := map[*List]string{l0: l0.String()}
	a := []*List{l0}
	rng := rand.New(rand.NewSource(time.Now().Local().UnixNano()))

	register := func(l *List) *List {
		switch e, ok := m[l]; {
		case ok:
			if g := l.String(); g != e {
				t.Fatalf("%v: %p %q %q", len(a), l, g, e)
			}
		default:
			m[l] = l.String()
			a = append(a, l)
		}
		return l
	}

	for len(a) < N {
		l := a[rng.Intn(len(a))]
		l2 := rndList(rng)
		register(l2)
		l3 := Snoc(l, l2)
		register(l3)
	}
	for l, e := range m {
		if g := l.String(); g != e {
			t.Fatalf("%v: %p %q %q", len(a), l, g, e)
		}
	}
}

func TestPersistent(t *testing.T) {
	const N = 1e6
	l0 := NewList()
	m := map[*List]string{l0: l0.String()}
	a := []*List{l0}
	rng := rand.New(rand.NewSource(time.Now().Local().UnixNano()))

	register := func(l *List) *List {
		switch e, ok := m[l]; {
		case ok:
			if g := l.String(); g != e {
				t.Fatalf("%v: %p %q %q", len(a), l, g, e)
			}
		default:
			m[l] = l.String()
			a = append(a, l)
		}
		return l
	}

	for len(a) < N {
		l := a[rng.Intn(len(a))]
		switch rng.Intn(12) {
		case 0: // snoc elem
			e := rndElement(rng)
			l2 := Snoc(l, e)
			register(l2)
		case 1: // snoc list
			l2 := rndList(rng)
			register(l2)
			l3 := Snoc(l, l2)
			register(l3)
		case 2: // append elem
			e := rndElement(rng)
			x, err := Append(l, e)
			if err != nil {
				t.Fatalf("%v: %p %[2]q %v: %v", len(a), l, e, err)
			}

			if l2, ok := x.(*List); ok {
				register(l2)
			}
		case 3: // append list
			l2 := rndList(rng)
			x, err := Append(l, l2)
			if err != nil {
				t.Fatalf("%v: %p %[2]q %v %p %[3]v: %v", len(a), l, l2, err)
			}

			register(x.(*List))
		case 4:
			Car(l)
		case 5:
			l2 := Cdr(l)
			register(l2)
		case 6: // cons elem
			e := rndElement(rng)
			l2 := Cons(e, l)
			register(l2)
		case 7: // cons list
			l2 := rndList(rng)
			register(l2)
			l3 := Cons(l2, l)
			register(l3)
		case 8:
			l2 := Last(l, 1)
			register(l2)
		case 9:
			l2 := Member(5, func(a, b interface{}) bool { return a == b }, l)
			register(l2)
		case 10:
			l2 := NewList(rndElement(rng))
			register(l2)
		case 11:
			l2 := Reverse(l)
			register(l2)
		}
	}
	for l, e := range m {
		if g := l.String(); g != e {
			t.Fatalf("%v: %p %q %q", len(a), l, g, e)
		}

		g := Length(l)
		n := 0
		for l := l; l != nil; l = Cdr(l) {
			n++
		}
		if g != n {
			t.Fatalf("%v: %p %q: %v %v", len(a), l, e, g, n)
		}
	}
}
