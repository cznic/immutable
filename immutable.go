// Copyright 2020 The Immutable Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package immutable implements Lisp-like immutable/persistent[0] list handling.
//
// Changelog
//
// 2020-10-13: v1.0.3
//
// Appending (Append, Snoc) and prepending (Cons) now all have O(1) complexity.
//
//	goos: linux
//	goarch: amd64
//	pkg: modernc.org/immutable
//	BenchmarkAppend/1e1-12         	  466135	      2345 ns/op	    1104 B/op	      24 allocs/op
//	BenchmarkAppend/1e2-12         	   65278	     19031 ns/op	   10448 B/op	     207 allocs/op
//	BenchmarkAppend/1e3-12         	    9202	    185956 ns/op	  102672 B/op	    2754 allocs/op
//	BenchmarkAppend/1e4-12         	     626	   2242423 ns/op	 1543898 B/op	   29763 allocs/op
//	BenchmarkAppend/1e5-12         	      48	  27036571 ns/op	16445313 B/op	  299773 allocs/op
//	BenchmarkAppend/1e6-12         	       5	 228436034 ns/op	160503088 B/op	 2999784 allocs/op
//	BenchmarkCons/1e1-12           	 1792572	       682 ns/op	     320 B/op	      10 allocs/op
//	BenchmarkCons/1e2-12           	  224952	      6639 ns/op	    3200 B/op	     100 allocs/op
//	BenchmarkCons/1e3-12           	   14814	     82928 ns/op	   37952 B/op	    1744 allocs/op
//	BenchmarkCons/1e4-12           	    1447	    873395 ns/op	  397953 B/op	   19744 allocs/op
//	BenchmarkCons/1e5-12           	     100	  10621615 ns/op	 3997970 B/op	  199744 allocs/op
//	BenchmarkCons/1e6-12           	      10	 110546361 ns/op	39998030 B/op	 1999744 allocs/op
//	BenchmarkSnoc/1e1-12           	  662372	      2081 ns/op	    1104 B/op	      24 allocs/op
//	BenchmarkSnoc/1e2-12           	   69583	     17483 ns/op	   10448 B/op	     207 allocs/op
//	BenchmarkSnoc/1e3-12           	    6681	    182784 ns/op	  102672 B/op	    2754 allocs/op
//	BenchmarkSnoc/1e4-12           	     512	   2246509 ns/op	 1543893 B/op	   29763 allocs/op
//	BenchmarkSnoc/1e5-12           	      42	  26167860 ns/op	16445317 B/op	  299773 allocs/op
//	BenchmarkSnoc/1e6-12           	       5	 222269167 ns/op	160503016 B/op	 2999783 allocs/op
//	PASS
//	ok  	modernc.org/immutable	28.445s
//
// The above numbers indicate the cost of a single-element Append or Snoc is
// about 200ns/160B and the cost of Cons is about 100ns/39B. Measured on an
// older Intel® Xeon(R) CPU E5-1650 v2 @ 3.50GHz × 12 machine.
//
// Links
//
// Referenced from elsewhere
//
//   [0]: https://en.wikipedia.org/wiki/Persistent_data_structure
package immutable

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

var (
	_     = trc
	_     = todo
	empty = &free{}
)

type free struct{ _ int }

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TODOTODO %s", origin(2), s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string { //TODO-
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

// List represents a Lisp-like list.
type List struct {
	car    interface{}
	cdr    *List
	append *[]interface{}
}

// Car returns the first element of l or nil when l is nil.
func Car(l *List) interface{} {
	if l != nil {
		return l.car
	}

	return nil
}

// Cdr returns l without its first element or nil when l is nil.
func Cdr(l *List) (r *List) {
	if l == nil {
		return nil

	}

	if l.cdr == nil {
		if l.append == nil {
			return nil
		}

		n := *l
		s := *l.append
		n.car = s[0]
		s = s[1:]
		switch {
		case len(s) != 0:
			n.append = &s
		default:
			n.append = nil
		}
		return &n

	}

	// l.cdr != nil
	cdr := l.cdr
	if l.append == nil {
		return cdr
	}

	// l.cdr != nil, l.append != nil
	n := *cdr
	if n.append == nil {
		n.append = l.append
		return &n
	}

	// l.cdr != nil, l.append != nil, cdr.append != nil
	s := *n.append
	if cap(s) > len(s) {
		s2 := s[:cap(s)]
		if s2[len(s)] != empty {
			s = s[:len(s):len(s)]
		}
	}
	s = append(s, *l.append...)
	if cap(s) > len(s) {
		s2 := s[:cap(s)]
		if s2[len(s)] == nil {
			for i := len(s); i < len(s2); i++ {
				s2[i] = empty
			}
		}
	}
	n.append = &s
	return &n
}

// Cons returns a list that is the result of inserting e as the first element of l.
func Cons(e interface{}, l *List) *List {
	return &List{e, l, nil}
}

// Snoc returns a list which has e as its last element.
func Snoc(l *List, e interface{}) (r *List) {
	if l == nil {
		return Cons(e, nil)
	}

	n := *l
	var s []interface{}
	if n.append != nil {
		s = *n.append
		if cap(s) > len(s) {
			s2 := s[:cap(s)]
			if s2[len(s)] != empty {
				s = s[:len(s):len(s)]
			}
		}
	}
	s = append(s, e)
	if cap(s) > len(s) {
		s2 := s[:cap(s)]
		if s2[len(s)] == nil {
			for i := len(s); i < len(s2); i++ {
				s2[i] = empty
			}
		}
	}
	n.append = &s
	return &n
}

// NewList returns a list constructed from args.
func NewList(args ...interface{}) (r *List) {
	for i := len(args) - 1; i >= 0; i-- {
		r = Cons(args[i], r)
	}
	return r
}

// String implements fmt.Stringer.
func (l *List) String() string {
	var b strings.Builder
	l.string(&b)
	return b.String()
}

func (l *List) string(b *strings.Builder) {
	b.WriteByte('[')
	var a []string
	for ; l != nil; l = Cdr(l) {
		a = append(a, fmt.Sprint(Car(l)))
	}
	b.WriteString(strings.Join(a, " "))
	b.WriteByte(']')
}

// Append merges its arguments into a list. All but the last arguments must be
// of type *List. Empty list arguments are ignored unless they are the last
// argument.
//
// No arguments
//
// Append() returns [].
//
// A single argument
//
// Append(anything) returns anything, unchanged.
func Append(args ...interface{}) (interface{}, error) {
	switch len(args) {
	case 0:
		return (*List)(nil), nil
	case 1:
		return args[0], nil
	}

	var l *List
	for i, v := range args {
		switch x := v.(type) {
		case *List:
			if x == nil {
				break
			}

			switch {
			case i == len(args)-1:
				if l == nil {
					return x, nil
				}

				for ; x != nil; x = Cdr(x) {
					l = Snoc(l, Car(x))
				}
			default:
				if l == nil {
					l = x
					break
				}

				for ; x != nil; x = Cdr(x) {
					l = Snoc(l, Car(x))
				}
			}
		default:
			switch {
			case i == len(args)-1:
				if l == nil {
					return x, nil
				}

				l = Snoc(l, x)
			default:
				return nil, fmt.Errorf("append: %v is not a list", x)
			}
		}
	}
	return l, nil
}

// Last returns a list of last n items in l. If n <= 0 the result is []. If n is
// greater or equal to the number of elements in l the result is l.
func Last(l *List, n int) *List {
	if l == nil || n <= 0 {
		return nil
	}

	last := make([]*List, n)
	len := 0
	i := 0
	for l := l; l != nil; l = Cdr(l) {
		last[i] = l
		len++
		i++
		if i == n {
			i = 0
		}
	}
	switch {
	case n >= len:
		return l
	default:
		return last[i]
	}
}

// Member searches the top level of l for a member that is equal to m and
// returns the tail of l starting with that member, or nil otherwise.
func Member(m interface{}, equal func(a, b interface{}) bool, l *List) *List {
	for ; l != nil; l = Cdr(l) {
		if equal(m, Car(l)) {
			return l
		}
	}
	return nil
}

// Reverse returns l with top level members in reverse order.
func Reverse(l *List) (r *List) {
	for ; l != nil; l = Cdr(l) {
		r = Cons(Car(l), r)
	}
	return r
}

// Length returns the number of top level members in l.
func Length(l *List) (r int) {
	for ; l != nil; l = l.cdr {
		r++
		if l.append != nil {
			r += len(*l.append)
		}
	}
	return r
}
