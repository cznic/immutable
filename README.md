# immutable

Package immutable implements Lisp-like immutable list handling.

Installation

    $ go get modernc.org/immutable

Documentation: [godoc.org/modernc.org/immutable](http://godoc.org/modernc.org/immutable)
